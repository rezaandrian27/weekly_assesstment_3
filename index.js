class Human {
  constructor(props) {
    if (this.constructor === Human) {
      throw new Error("Cannot instantiate from Abstract Class");
    }

    let { name, address } = props;
    this.name = name;
    this.address = address;
    this.profesion = this.constructor.name;
  }

  work() {
    console.log("Working...");
  }

  introduce() {
    console.log(`Hello, my name is ${name}`);
  }
}

class Human {
  constructor(props) {
    if (this.constructor === Human) {
      throw new Error("Cannot instantiate from Abstract Class"); // Because it's abstract
    }

    let { name, address } = props;
    this.name = name; // Every human has name
    this.address = address; // Every human has address
    this.profession = this.constructor.name; // Every human has profession, and let the child class to define it.
  }

  // Yes, every human can work
  work() {
    console.log("Working...");
  }

  // Every human can introduce
  introduce() {
    console.log(`Hello, my name is ${name}`);
  }
}

class Police extends Human {
  constructor(props) {
    super(props);
    this.rank = props.rank; // Add new property, rank.
  }

  work() {
    console.log("Go to the police station");
    super.work();
  }
}

const Wiranto = new Police({
  name: "Wiranto",
  address: "Unknown",
  rank: "General",
});

console.log(Wiranto.profession); // Police

try {
  let Abstract = new Human({
    name: "Abstract",
    address: "Unknown",
  });
} catch (err) {
  console.log(err.message); // Cannot instantiate from Abstract Class
}

class Person {
  constructor({ firstName, lastName, job }) {
    this.firstNames = firstName;
    this.lastName = lastName;
    this.job = job;
    this.skills = [];
    Person._amount = Person._amount || 0;
    Person._amount++;
  }

  static get amount() {
    return Person._amount;
  }

  get fullName() {
    return `${this.firstNames} ${this.lastName}`;
  }

  set fullName(fN) {
    if (/[A-Za-z]\s[A-Za-z]/.test(fN)) {
      [this.firstNames, this.lastName] = fN.split(" ");
    } else {
      throw Error("Bad fullname");
    }
  }

  learn(skill) {
    return this.skills.push(skill);
  }
}

class Job {
  constructor(company, position, salary) {
    this.company = company;
    this.position = position;
    this.salary = salary;
  }
}

const john = new Person({
  firstName: "John",
  lastName: "Doe",
  job: new Job("Youtube", "developer", 200000),
});

const roger = new Person({
  firstName: "Roger",
  lastName: "Federer",
  job: new Job("ATP", "tennis", 1000000),
});

console.log(john.firstNames); //output: John
console.log(john.lastName); //output: Doe
console.log(john.fullName);
john.fullName = "Mike Smith";
console.log(john.fullName); //output: Mike Smith
console.log(john.firstNames); //output: Mike
console.log(john.lastName); //output: Smith

john.learn("es6");
john.learn("es7");
console.log(john.skills); //output: ["es6","es7"]

roger.learn("programming");
console.log(roger.skills); //output: ["programming"]

class Person {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }
}

// Create a child class from Person
class Artist extends Person {
  constructor(name, address, Artist) {
    super(name, address); // Call the super/parent class constructor, in this case Person.constructor;
    this.Artist = Artist;
  }

  // Overload the Introduce Method
  introduce(withDetail) {
    super.introduce(); // Call the super class introduce instance method.
    Array.isArray(withDetail) ? console.log(`I Used to playing role play on movie as an ${this.Artist}`) : console.log("Wrong input");
  }

  code() {
    let acak = Math.floor(Math.random() * this.Artist.length);
    console.log("Code some", this.Artist[acak]);
  }
}

let Raissa = new Artist("Raissa", "Jakarta", ["Art", " Musician"]);
Isyana.Raissa(["Artist"]);
Raissa.code(); //Code some ...

class Family {
  constructor(Husband, Wife) {
    this.name = Husband;
    this.address = Wife;
  }

  introduce() {
    console.log(`This is our happy family! with my ${Husband}`);
  }

  House() {
    console.log("Home!");
  }
}

class Child extends Family {
  constructor(Husband, wife, boy, girl) {
    super(Husband, wife);
    this.son = boy;
    this.daughter = girl;
  }

  introduce() {
    super.introduce();
    console.log(`In my family we have ${this.son} and 2 ${this.daughter}`);
  }
}

class Person {
  constructor(name, job) {
    this.name = name;
    this.address = job;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }
}

class Artist extends Person {
  constructor(name, job, Artist) {
    super(name, job); // Call the super/parent class constructor, in this case Person.constructor;
    this.Artist = Artist;
  }

  // Override the Introduce Method
  introduce() {
    super.introduce(); // Call the super class introduce instance method.
    console.log(`I'm an Artist who played many movie'`, this.Artist);
  }

  code() {
    console.log("Code some", this.Artist[Math.floor(Math.random * this.Artist.length)]);
  }
}

let Isyana = new Artist("Raissa Anggiani", "Jakarta", ["Movie", "Sports", "News", "Musician"]);
Isyana.introduce();
